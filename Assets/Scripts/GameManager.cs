﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
	public static GameManager instance;
	public int playerScore;
    public int EnemyScore;
    public int match = 0;

	void Awake()
	{
		if (instance == null)
		{
			DontDestroyOnLoad(this.gameObject);
			instance = this;
		}

		else if (instance != this)
		{
			Destroy(gameObject);
			return;
		}
	}

}
