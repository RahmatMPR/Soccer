﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class NextTo : MonoBehaviour
{
    public void nextScene(string sceneName)
    {
        if(sceneName == "NewGameplay")
        {
            GameManager gm = FindObjectOfType<GameManager>();
            gm.EnemyScore = 0;
            gm.playerScore = 0;
            gm.match = 0;
        }
        SceneManager.LoadScene(sceneName);
    }
}
