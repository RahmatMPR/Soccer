﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenderController : MonoBehaviour
{

    [Header("Defender Child Object")]
    [SerializeField] GameObject DetectionField;
    public RotateTo center;
    [Space(15)]

    [Header("Defender Target Component")]
    public GameObject enemyTarget;
    public bool caseEnemy = false;
    public string targetTag;
    [Space(15)]

    [Header("Defender Component")]
    PlayerManager pm;
    public bool isPlayer;
    [SerializeField] Color tempColor;
    [SerializeField] Color deactiveColor;
    [SerializeField] GameObject detector;
    public float speed;
    [Space(15)]

    Rigidbody myRb;
    Collider mycollider;
    MeshRenderer myMaterial;
    bool firstTime = true;
    bool backToOrigin = false;
    float reactive = 4.0f;
    float reacTemp;
    Vector3 startPosition;
    private float firstreactive;

    //Transform startPosTemp;

    void rotationToObject()
    {
        if (backToOrigin)
        {
            Vector3 TargetPosition = startPosition;
            center.transform.LookAt(TargetPosition, Vector3.up);
        }
        if (!backToOrigin)
        {
            Vector3 TargetPosition = new Vector3(center.target.transform.position.x, transform.position.y, center.target.transform.position.z);
            center.transform.LookAt(TargetPosition, Vector3.up);
        }
    }
    void MoveToEnemy()
    {
        var step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(enemyTarget.transform.position.x,
            transform.position.y,
            enemyTarget.transform.position.z), step);
    }

    void MoveToOriginPos()
    {
        var step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, startPosition, step);
    }

    IEnumerator setmaterialAlpha(GameObject obj)
    {
        MeshRenderer renderer = obj.GetComponent<MeshRenderer>();
        float alphaPerSec = 0.7f;
        while (true)
        {
            float alpha = renderer.material.color.a + (alphaPerSec*Time.deltaTime);
            if (alpha <= 0.0f)
                alphaPerSec = 0.7f;
            if (alpha >= 0.5f)
                alphaPerSec = -0.7f;
            yield return null;
            renderer.material.color = new Color(renderer.material.color.r, renderer.material.color.g, 
                renderer.material.color.b, alpha);
        }
    }

    void setActiveComponent(bool set)
    {
        mycollider.enabled = set;
        myRb.useGravity = set;
        detector.SetActive(set);
        if (set)
        {
            myMaterial.material.color = tempColor;
        }
        if (!set)
        {
            myMaterial.material.color = deactiveColor;
        }
    }
    void getObjComponent()
    {
        myMaterial = gameObject.GetComponent<MeshRenderer>();
        mycollider = gameObject.GetComponent<Collider>();
        myRb = gameObject.GetComponent<Rigidbody>();
        tempColor = myMaterial.material.color;
        reacTemp = reactive;
    }
    void getPlayerManager()
    {
        PlayerManager[] playerManagers;
        playerManagers = new PlayerManager[2];
        playerManagers = FindObjectsOfType<PlayerManager>();
        for (int i = 0; i < playerManagers.Length; i++)
        {
            if (isPlayer == playerManagers[i].isPlayer)
            {
                pm = playerManagers[i];
            }
            else
            {

            }
        }

    }
    // Start is called before the first frame update
    void Start()
    {
        getObjComponent();
        getPlayerManager();
        startPosition = gameObject.transform.position;
        center.gameObject.SetActive(false);
        StartCoroutine(setmaterialAlpha(DetectionField));
    }

    // Update is called once per frame
    void Update()
    {
        if (!pm.gpm.pause)
        {
            if (firstTime)
            {

                setActiveComponent(false);
                firstreactive -= Time.deltaTime;
                if (firstreactive <= 0.0f)
                {
                    speed = 1;
                    setActiveComponent(true);
                    detector.SetActive(true);
                    reactive = reacTemp;
                    firstTime = false;
                }
            }
            if (!firstTime)
            {
                if (caseEnemy)
                {
                    MoveToEnemy();
                    rotationToObject();
                }
                if (backToOrigin)
                {
                    setActiveComponent(false);
                    MoveToOriginPos();
                    rotationToObject();

                    reactive -= Time.deltaTime;
                    if (reactive <= 0.0f)
                    {
                        speed = 1;
                        setActiveComponent(true);
                        center.gameObject.SetActive(false);
                        reactive = reacTemp;
                        backToOrigin = false;
                    }
                }
            }

        }

    }
    private void OnCollisionEnter(Collision coll)
    {
        if(coll.gameObject.tag == targetTag)
        {
            AttackerController atr = coll.gameObject.GetComponent<AttackerController>();
            if (atr.igotBall)
            {
                caseEnemy = false;
                backToOrigin = true;
                //Destroy(coll.gameObject);
                atr.stoped = true;
                atr.igotBall = false;
                speed = 2;
            }
        }
        
    }
}
