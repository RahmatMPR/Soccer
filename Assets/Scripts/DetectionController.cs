﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectionController : MonoBehaviour
{
    [SerializeField]DefenderController def;
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == def.targetTag)
        {
            AttackerController atr = other.gameObject.GetComponent<AttackerController>();
            if (atr.igotBall)
            {
                def.caseEnemy = true;
                def.enemyTarget = other.gameObject; 
                def.center.gameObject.SetActive(true);
                def.center.target = other.gameObject.transform;
            }
        }
    }
}
