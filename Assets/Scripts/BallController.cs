﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    public GameObject target;
    public GameObject myParent;
    public bool passed = true;
    Rigidbody rb;
    float speed = 1.5f;
    PlayerManager pm;

    public void setManager(PlayerManager playerManager)
    {
        pm = playerManager;
    }
    public void setRecieverToWait(AttackerController atr)
    {
        atr.wait = true;
    }
    public void MoveTo()
    {
        var step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(target.transform.position.x,
            transform.position.y,
            target.transform.position.z), step);
    }
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (!passed)
        {
            MoveTo();
            if (target == null)
            {
                pm.hadball = false;
                pm.target = gameObject;
                //passed = true;
            }
        }

    }
}
