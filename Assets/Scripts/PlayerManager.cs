﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct playerComponent
{
}

public class PlayerManager : MonoBehaviour
{
    public bool isAttack = false;
    public bool isPlayer;
    float TotalEnergy = 6;
    public Slider EnergySlider;
    public float currentEnergy = 0;
    public float EnergyPerSecond = 0.5f;
    public Text ScoreText;
    public bool hadball = false;
    public float attackerCost = 2.0f;
    public float defenderCost = 3.0f;
    public float cost;
    public GameplayManager gpm;
    public List<GameObject> playerPion;
    public GameObject target;

    public void changeAlltarget(GameObject Target)
    {
        for (int i = 0; i <playerPion.Count; i++)
        {
            AttackerController atr = playerPion[i].GetComponent<AttackerController>();
            atr.target = Target.transform;
            atr.center.target = Target.transform;
        }
    }

    private void Awake()
    {
        target = GameObject.Find("Ball");
    }
    // Start is called before the first frame update
    void Start()
    {
        
        gpm = FindObjectOfType<GameplayManager>();
        playerPion = new List<GameObject>();
        EnergySlider.value = currentEnergy;
        EnergySlider.maxValue = TotalEnergy;
    }

    // Update is called once per frame
    void Update()
    {
        if(!gpm.pause)
            if (currentEnergy < TotalEnergy)
            {
                currentEnergy += Time.deltaTime * EnergyPerSecond;
                EnergySlider.value = currentEnergy;
                //EnergyText.text = "" + currentEnergy;
            }
    }
}
