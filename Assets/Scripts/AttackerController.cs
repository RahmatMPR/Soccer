﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackerController : MonoBehaviour
{
    [Header("Attacker Child Object")]
    public RotateTo center;
    public Transform target;
    public GameObject HighligtPos;
    [Space(15)]

    [Header("Attacker Component")]
    public PlayerManager pm;
    public bool isPlayer;
    public bool stoped = false;
    public bool igotBall = false;
    public bool wait = false;
    [SerializeField] string goalName;
    [SerializeField] Color tempColor;
    [SerializeField] Color deactiveColor;
    [SerializeField] GameObject ball;
    [Space(15)]

    bool firstTime = true;
    bool passed = false;
    
    float speed = 1.5f;
    float reactive = 2.5f;
    float reacTemp;
    MeshRenderer myMaterial;
    Collider mycollider;
    Rigidbody myRb;
    private float firstReactive=0.5f;

    public void MoveToBall()
    {
        var step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(target.transform.position.x,
            transform.position.y,
            target.transform.position.z), step);
    }
    public void MoveToEnemyBase()
    {
        var step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(target.transform.position.x,
            transform.position.y,
            transform.position.z), step);
        if (transform.position.x == target.transform.position.x)
        {
            BallController bl = ball.GetComponent<BallController>();
            if (bl.passed)
            {
                if (igotBall)
                {
                    BallController ballCon = ball.GetComponent<BallController>();
                    ballCon.passed = true;
                    Rigidbody ballRb = ball.GetComponent<Rigidbody>();
                    ballRb.useGravity = true;
                    ball.transform.SetParent(GameObject.Find("Plane Object").transform);
                    if (pm.isPlayer)
                    {
                        ball.transform.position = new Vector3(pm.gpm.playerZone.position.x + (Random.Range(0, 2.5f)), transform.position.y, pm.gpm.playerZone.position.z + (Random.Range(-4.0f, 4.0f)));
                    }
                    if (!pm.isPlayer)
                    {
                        ball.transform.position = new Vector3(pm.gpm.enemyZone.position.x + (Random.Range(0, 2.5f)), transform.position.y, pm.gpm.enemyZone.position.z + (Random.Range(-4.0f, 4.0f)));
                    }
                    pm.changeAlltarget(GameObject.Find("Ball"));
                    pm.gpm.addScore(pm);
                    pm.hadball = false;
                }
            }
            if (!wait)
            {
                pm.playerPion.Remove(gameObject);
                Destroy(gameObject);
            }
        }
    }

    void FindTarget(string objName)
    {
        target = GameObject.Find(objName).GetComponent<Transform>();
        center.target = target;
    }
    void rotationToObject()
    {
        if (!pm.hadball)
        {
            Vector3 TargetPosition = new Vector3(center.target.transform.position.x, transform.position.y, center.target.transform.position.z);
            center.transform.LookAt(TargetPosition, Vector3.up);
        }
        if (pm.hadball)
        {
            Vector3 TargetPosition = new Vector3(center.target.transform.position.x, transform.position.y,transform.position.z);
            center.transform.LookAt(TargetPosition, Vector3.up);
        }

    }
    void setActiveComponent(bool set)
    {
        mycollider.enabled = set;
        myRb.useGravity = set;
        if (set)
        {
            myMaterial.material.color = tempColor;
        }
        if (!set)
        {
            myMaterial.material.color = deactiveColor;
        }
    }

    void getObjComponent()
    {
        myMaterial = gameObject.GetComponent<MeshRenderer>();
        mycollider = gameObject.GetComponent<Collider>();
        myRb = gameObject.GetComponent<Rigidbody>();
        tempColor = myMaterial.material.color;
        reacTemp = reactive;
    }

    void getPlayerManager()
    {
        PlayerManager[] playerManagers;
        playerManagers = new PlayerManager[2];
        playerManagers = FindObjectsOfType<PlayerManager>();
        for (int i = 0; i < playerManagers.Length; i++)
        {
            if (isPlayer == playerManagers[i].isPlayer)
            {
               pm = playerManagers[i];
            }
        }

    }
    void SetToPass()
    {
        bool first= true;
        float distance = 0;
        float closestDistance = 0;
        if (pm.playerPion.Count >= 2)
        {   
            for (int i = 0; i < pm.playerPion.Count; i++)
            {
                AttackerController atr = pm.playerPion[i].gameObject.GetComponent<AttackerController>();
                if (pm.playerPion[i].gameObject != gameObject)
                {
                    if (!atr.stoped)
                    {
                        distance = Vector3.Distance(
                        gameObject.transform.position, pm.playerPion[i].gameObject.transform.position);
                        Debug.Log("distance between " + pm.playerPion[i].name + "and " + gameObject.name + " is " + distance);
                        if (first)
                        {
                            closestDistance = distance;
                            BallController ballCon = ball.GetComponent<BallController>();
                            ballCon.target = pm.playerPion[i];
                            first = false;
                        }
                        if (!first)
                        {
                            if (distance < closestDistance)
                            {
                                closestDistance = distance;
                                BallController ballCon = ball.GetComponent<BallController>();
                                ballCon.target = pm.playerPion[i];
                            }
                        }
                    }
                }
            }
            PassTo();
            Debug.Log("closest Distance : " + closestDistance);

        }
        else
        {
            BallController ballCon = ball.GetComponent<BallController>();
            if (pm.isPlayer)
            {
                ballCon.target = GameObject.Find(pm.gpm.playerZone.name);
                
             
            }
            if (!pm.isPlayer)
            {
                ballCon.target = GameObject.Find(pm.gpm.enemyZone.name);
            }
            PassTo();
            pm.changeAlltarget(ball);
            pm.hadball = false;
        }
    }
    void PassTo()
    {
        BallController ballCon = ball.GetComponent<BallController>();
        ballCon.passed = false;
        ballCon.setRecieverToWait(ballCon.target.GetComponent<AttackerController>());
        wait = false;
        igotBall = false;
        Rigidbody ballRb = ball.GetComponent<Rigidbody>();
        ballRb.useGravity = true;
        ball.transform.SetParent(GameObject.Find("Plane Object").transform);
        
    }
    // Start is called before the first frame update
    void Start()
    {
        getObjComponent();
        getPlayerManager();
        FindTarget(pm.target.name);
    }

    // Update is called once per frame
    void Update()
    {
        if (!pm.gpm.pause)
        {
            if (firstTime)
            {
                setActiveComponent(false);
                firstReactive -= Time.deltaTime;
                if (firstReactive <= 0.0f)
                {
                    setActiveComponent(true);
                    reactive = reacTemp;
                    firstTime = false;
                }
            }
            if (!firstTime)
            {
                if (!stoped)
                {
                    rotationToObject();
                    if (!pm.hadball)
                        MoveToBall();
                    if (pm.hadball)
                        MoveToEnemyBase();
                    if (!igotBall)
                        speed = 1.5f;
                    if (igotBall)
                        speed = 0.75f;
                }
                if (stoped)
                {
                    if (!passed)
                    {
                        SetToPass();
                        passed = true;
                    }
                    setActiveComponent(false);
                    reactive -= Time.deltaTime;
                    if (reactive <= 0.0f)
                    {
                        passed = false;
                        setActiveComponent(true);
                        reactive = reacTemp;
                        stoped = false;
                    }
                }
            }
        }

    }   

    

    private void OnCollisionEnter(Collision coll)
    {
        if(coll.gameObject.name == "Ball")
        {
            igotBall = true;
            pm.hadball = true;
            wait = false;
            FindTarget(goalName);
            pm.changeAlltarget(GameObject.Find(goalName));
            ball = coll.gameObject;
            BallController ballCon = ball.GetComponent<BallController>();
            ballCon.passed = true;
            coll.gameObject.transform.SetParent(HighligtPos.transform);
            coll.gameObject.transform.position =new Vector3(HighligtPos.transform.position.x, HighligtPos.transform.position.y,HighligtPos.transform.position.z);
            Rigidbody rb = coll.gameObject.GetComponent<Rigidbody>();
            rb.useGravity = false;
        }
    }
}
